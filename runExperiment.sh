# Run the experiment to validate the results of "Open issues in router buffer sizing"
# First, we set up all the commands. Then we run them

if [ -e "$1" ]
then
    echo "Found manifest: $1"
else
    echo "No manifest found at $1"
    exit 1
fi

# Parse manifest to get login information for each host
sinkSshCmd=$(python parseManifest.py "$1" "$2" sink)
targetSshCmd=$(python parseManifest.py "$1" "$2" target2)
sourcesCsshCmd=$(python parseManifest.py "$1" "$2" sources)
scpCmd=$(python parseManifest.py "$1" "$2" scp)

# Get name of interface on target2 that connects to target1 and the sink
targetIfaceCmd="$targetSshCmd 'echo \$(ip route get 10.20.1.255 | cut -f4 -d\" \") ' | tail -n 1"
targetIface=`eval $targetIfaceCmd`

# Set up command to configure buffer for Bs
smallBuffCmd="$targetSshCmd"
smallBuffCmd+=" '"
smallBuffCmd+="sudo tc qdisc replace dev $targetIface root netem limit 18 delay 5ms"
smallBuffCmd+="'"

# Set up command to configure buffer for Bl
largeBuffCmd="$targetSshCmd"
largeBuffCmd+=" '"
largeBuffCmd+="sudo tc qdisc replace dev $targetIface root netem limit 500 delay 5ms"
largeBuffCmd+="'"

# Set up commands to start the receiver on sink, for each of two scenarios and two buffer sizes
#small buffer experiment persistent connection
rcvPerCmdSmall="$sinkSshCmd 'nohup bash /receiverPer.sh > receiverPerSmall.out 2> receiverPerSmall.err < /dev/null &'"
#large buffer experiment persistent connection
rcvPerCmdLarge="$sinkSshCmd 'nohup bash /receiverPer.sh > receiverPerLarge.out 2> receiverPerLarge.err < /dev/null &'"
#small buffer closedloop
rcvCLCmdSmall="$sinkSshCmd 'nohup bash /receiverCL.sh > receiverCLSmall.out 2> receiverCLSmall.err < /dev/null &'"
#large buffer closedloop
rcvCLCmdLarge="$sinkSshCmd 'nohup bash /receiverCL.sh > receiverCLLarge.out 2> receiverCLLarge.err < /dev/null &'"

# Set up commands to start tcpdumps and traffic generators for each of two scenarios and two buffer sizes
#Persistent connection
srcPerTCPdumpSmall="$targetSshCmd 'sudo killall tcpdump; screen -X -S sleepy quit; screen -S perConnection -dm sh -c \"sudo tcpdump -U -n -i $targetIface -w perConnectionSmall.pcap\"'"
srcPerSmallCmd="$sourcesCsshCmd --action 'bash /Perflow.sh | tee out-small-per.txt'"
srcPerTCPdumpLarge="$targetSshCmd 'sudo killall tcpdump; screen -X -S sleepy quit; screen -S perConnection -dm sh -c \"sudo tcpdump -U -n -i $targetIface -w perConnectionLarge.pcap\"'"
srcPerLargeCmd="$sourcesCsshCmd --action 'bash /Perflow.sh | tee out-large-per.txt'"
#closed loop connection
srcCLTCPdumpSmall="$targetSshCmd 'sudo killall tcpdump; screen -X -S sleepy quit; screen -S perConnection -dm sh -c \"sudo tcpdump -U -n -i $targetIface -w CLConnectionSmall.pcap\"'"
srcCLSmallCmd="$sourcesCsshCmd --action 'bash /CLflow.sh | tee out-small-cl.txt'"
srcCLTCPdumpLarge="$targetSshCmd 'sudo killall tcpdump; screen -X -S sleepy quit; screen -S perConnection -dm sh -c \"sudo tcpdump -U -n -i $targetIface -w CLConnectionLarge.pcap\"'"
srcCLLargeCmd="$sourcesCsshCmd --action 'bash /CLflow.sh | tee out-large-cl.txt'"

# Set up commands to kill lingering processes at the end of each experiment
perfKillCmd="$sinkSshCmd 'sudo killall \"iperf\"'"
tcpdumpKillCmd="$targetSshCmd 'sudo killall \"tcpdump\"'"

# Make sure all the nodes are set up:
for host in "h1g1" "h1g3" "h1g2" "h2g10" "h2g11" "h2g31" "h2g30" "h2g20" "h3g20" "h3g21" "h3g10" "h3g11" "h3g51" "h3g41" "h3g50" "h3g40" "h3g30" "h3g31" "target2"; do
	echo "Checking if $host is ready..."
	remoteSshCmd=$(python parseManifest.py "$1" "$2" "$host")
	checkSetupCmd="$remoteSshCmd 'ls / | grep flowPer | wc -l'"
	isSetup=`eval $checkSetupCmd`
	if [[ $isSetup -eq 0 ]] ; then
		echo "Not all hosts are configured yet (e.g. $host) - wait a little longer before trying again"
    		exit 1
	fi
done

for host in "sink"; do
        echo "Checking if $host is ready..."
        remoteSshCmd=$(python parseManifest.py "$1" "$2" "$host")
        checkSetupCmd="$remoteSshCmd 'ls / | grep receiverPer | wc -l'"
        isSetup=`eval $checkSetupCmd`
        if [[ $isSetup -eq 0 ]] ; then
                echo "Not all hosts are configured yet (e.g. $host) - wait a little longer before trying again"
                exit 1
        fi
done

echo "OK! All hosts are ready, starting experiments."
echo "**********************************************"


# Run experiments
#Persistent connection
# Small buffer size, persistent connections with mice
echo "Running: persistent connections with mice, small buffer size"
eval "$srcPerTCPdumpSmall"
sleep 5
eval "$smallBuffCmd"
eval "$rcvPerCmdSmall"
eval "$srcPerSmallCmd"
eval "$perfKillCmd"
eval "$tcpdumpKillCmd"

sleep 20

# Large buffer size, persistent connections with mice
echo "Running: persistent connections with mice, large buffer size"
eval "$srcPerTCPdumpLarge"
sleep 5
eval "$largeBuffCmd"
eval "$rcvPerCmdLarge"
eval "$srcPerLargeCmd"
eval "$perfKillCmd"
eval "$tcpdumpKillCmd"

sleep 20

#closed Loop Connection
# Small buffer size, closed loop
echo "Running: closed loop traffic, small buffer size"
eval "$srcCLTCPdumpSmall"
sleep 5
eval "$smallBuffCmd"
eval "$rcvCLCmdSmall"
eval "$srcCLSmallCmd"
eval "$perfKillCmd"
eval "$tcpdumpKillCmd"

sleep 20

# Large buffer size, closed loop
echo "Running: closed loop traffic, large buffer size"
eval "$srcCLTCPdumpLarge"
sleep 5
eval "$largeBuffCmd"
eval "$rcvCLCmdLarge"
eval "$srcCLLargeCmd"
eval "$perfKillCmd"
eval "$tcpdumpKillCmd"

sleep 20

# Data analysis
echo "Completed all experiments - running data analysis"
RscriptRunning="$targetSshCmd 'tshark -T fields -n -r perConnectionSmall.pcap -e frame.time_epoch -e ip.src -e ip.dst -e tcp.srcport -e tcp.dstport -e frame.len -e tcp.analysis.retransmission -E separator=, > perConnectionSmall.csv; tshark -T fields -n -r perConnectionLarge.pcap -e frame.time_epoch -e ip.src -e ip.dst -e tcp.srcport -e tcp.dstport -e frame.len -e tcp.analysis.retransmission -E separator=, > perConnectionLarge.csv; tshark -T fields -n -r CLConnectionSmall.pcap -e frame.time_epoch -e ip.src -e ip.dst -e tcp.srcport -e tcp.dstport -e frame.len -e tcp.analysis.retransmission -E separator=, > CLConnectionSmall.csv; tshark -T fields -n -r CLConnectionLarge.pcap -e frame.time_epoch -e ip.src -e ip.dst -e tcp.srcport -e tcp.dstport -e frame.len -e tcp.analysis.retransmission -E separator=, > CLConnectionLarge.csv; sudo mv /dataAnalysis.R ~; Rscript dataAnalysis.R'"

eval "$RscriptRunning"

mkdir data
cd data
# Transfer files to "data" directory locally
eval "$scpCmd"
echo "Look for output in data/ folder"
