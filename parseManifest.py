import xmltodict
import sys

if len(sys.argv)!=4:
  print("\nThis script gives you the clusterssh command and SCP commands for the ")
  print("experiment on reproducing 'Open Issues in Router Buffer Sizing'.\n\n")
  print("Usage: python %s /path/to/manifest/rspec geniUsername cmd\n" % sys.argv[0])
  print("where 'cmd' is either: target2, sink, sources, or scp\n")
  sys.exit()

filename=sys.argv[1]
username=sys.argv[2]
cmdtype=sys.argv[3]

with open(filename) as fd:
    doc = xmltodict.parse(fd.read())

if cmdtype=="sources":
  sys.stdout.write('cssh -o "-o StrictHostKeyChecking=no" --username ')
  sys.stdout.write(username)

  for d in doc['rspec']['node']:
    if d['@client_id'].startswith("h"):
      sys.stdout.write(' ')
      sys.stdout.write(d['services']['login'][0]['@hostname'])
      sys.stdout.write(':')
      sys.stdout.write(d['services']['login'][0]['@port'])

if cmdtype=="scp":
  for d in doc['rspec']['node']:
    for f in ["tput-per", "loss-per", "tput-cl", "loss-cl"]:
      if d['@client_id'].endswith("t2"):
        sys.stdout.write('scp -o "StrictHostKeyChecking no" -P ')
        sys.stdout.write(d['services']['login'][0]['@port'])
        sys.stdout.write(' ')
        sys.stdout.write(username)
        sys.stdout.write('@')
        sys.stdout.write(d['services']['login'][0]['@hostname'])
        print(":~/%s* %s-%s.pdf " % (f, f, d['@client_id']))

if (cmdtype=="target2" or cmdtype=="sink" or cmdtype.startswith("h")):
  sys.stdout.write('ssh -o "StrictHostKeyChecking no" ')
  sys.stdout.write(username)
  sys.stdout.write('@')
  for d in doc['rspec']['node']:
    if d['@client_id']==cmdtype:
      sys.stdout.write(d['services']['login'][0]['@hostname'])
      sys.stdout.write(' -p ')
      sys.stdout.write(d['services']['login'][0]['@port'])

