Authors in [3] have simulated considered topology using network simulator 2. In this work, we have simulated the considered topology using GENI testbed to evaluate the real-time system performance. After, initial set-up simulation and analysis of results can take 3-4 hours.


Research results have suggested that a very small router buffer is sufficient, without causing a utilization loss as long as the link carries many TCP flows. Authors in “Open Issues in Router Buffer Sizing” argued that utilization, packet-loss rate, queuing delay are parameter for aggregate traffic analysis for network layer performance and they do not model performance of individual flow. Authors considered three different type of traffic model and showed that with different traffic model and flow size performance of individual TCP performance perform differently. Simulation was carried out using network simulator-2 in the reference paper. We have extended this work on Global Environment for Network Innovations (GENI) testbed to reproduce the result. Our results varies from the results in “Open Issues in Router Buffer Sizing” with certain extent. 


## Background

Finding optimal buffer size for routers has been a subject of discussion for a long time. Overly large buffer sizes can lead to excessive delay, while buffers that are too small lead to packet loss, which in turn reduces throughput and link utilization. 

Traditionally, the rule of thumb has been to set the router buffer size to some multiple of the bandwidth-delay product [[1](#references)]: the product of the bandwidth _C_ of the bottleneck link and the longest Round-Trip Time (RTT) _T_ experienced by a TCP connection that uses this link. However, in 2004 Appenzeller, Keslassy, and McKeown argued that buffer sizes much smaller than the bandwidth delay product can still achieve high link utilization if the number of flows is very large [[2](#references)], an approach known as the “Standford Model”. According to this model, the minimum buffer size _B_ required to saturate bottleneck link is given by

$$B = \frac{CT}{\sqrt{N}}$$

where _N_ is the number of large TCP flows at that link. Thus, the buffer size decreases inversely with the number of long TCP flows. However, Dhamdhere and Dovrolis challenge the Stanford Model, arguing that such small buffers lead to high packet loss [[3](#references)]. Furthermore, TCP throughput may suffer significantly due to packet losses since TCP throughput can be approximated by [[4](#references)]

$$R = \frac{0.87}{T\sqrt{p}}$$

where _p_ is the packet loss ratio of the link.


## Results

We have reproduced two experiments from Dhamdhere and Dovrolis, with two traffic models:

1. Persistent connections (long-lived flows) running together with mice (short-lived flows)
2. Closed-loop traffic, which is supposed to model heavy-tailed traffic with realistic flow sizes and waiting time between flows

For each experiment, we separately measure the CDF of per-flow throughput and the CDF of per-flow packet loss ratio for the large flows (> 200 KB) and small flows (< 200 KB). The figures below show the simulation results of Dhamdhere and Dovrolis on the right, and my testbed reproduction on the left.

### Persistent connections with mice 

For the experiment with persistent connections with mice, we have validated the results of Dhamdhere and Dovrolis for packet loss ratio for both large flows and short flows. We saw much greater packet loss with small buffer sizes (Stanford model):

![pic2.png](/blog/content/images/2016/05/pic2.svg)

However, we do not see the effect on throughput reported by Dhamdhere and Dovrolis. In our testbed experiment, both small and large buffer sizes yield similar throughput distributions:

![pic1.png](/blog/content/images/2016/06/pic1.svg)

We believe this may be because the smaller queueing delay in the small-buffer case compensates for its higher packet loss.



#### Closed loop traffic

In the closed-loop traffic experiment, our testbed results are similar to the simulation results reported by Dhamdhere and Dovrolis for both packet loss and throughput:

![pic4.png](/blog/content/images/2016/05/pic2-1.svg)

![pic3.png](/blog/content/images/2016/05/pic1-1.svg)



 

## 2. Background

I have reproduced Figure 2, Figure 3, Figure 4 and, Figure 5 using GENI testbed simulations in this work. Authors in [3] showed that utilization, loss-rate and queuing delay are metric only for aggregate analysis, and individual TCP flow performance can not be evaluated using these parameters. Authors have considered three different situation (i) persistent flow with small mice, (ii) closed loop traffic: where a TCP flow transmit packets and go back to sleep for some time (iii) open loop traffic. We have simulated TCP flows on GENI testbed for performance evaluation of individual flow using iperf3 as traffic generator. I expect variation from the results presented in [3], as we have implemented on a testbed. I will discuss similarities and difference between obtain results in coming sections.

## 3. Experiment Design
As describe in our referenced paper, we have consider a topology consisting of 18 nodes arranged in balanced-binary tree. Multiple flows originate from different source nodes having same destination address as sink. All the traffic in the topology is routed through a bottleneck link having link capacity as 50 Mbps. The tree- topology results in heterogeneous RTT in the range of 30ms-530ms with an effective RTT of _T<sub>eff</sub>_ = 60ms. Capacities and buffers of the access links are kept sufficiently high such that, only bottleneck in the topology is target link. Bottleneck link is the only link in the topology which saturate. Packet size for all flows are kept constant as 1500 bytes with _CT<sub>eff</sub>_ = 250 packets. The considered topology is illustrated in the Figure below.

#### Topology 

![topology.png](https://bitbucket.org/repo/xkxRKb/images/3269927985-topology.png)

### workload
We have consider two type of traffic in our simulations (i) Persistent connection with mice and, (ii) closed loop traffic. A mice is TCP flow consisting of small number of packets. Persistent connections are keep-alive connection for sending multiple requests and replies. In research community for analytical study and simulation, Poisson arrival process and exponential departure time are widely considered. Persistent connection models this type of traffic. However, in practice INTERNET traffic follow a heavy- tailed size distribution and their number fluctuates significantly with time. We have considered closed loop flow having heavy-tail distribution. The description of the workload in these conditions are,

#### Persistent TCP flows with small fraction of mice
We have created 200 persistent connection starting from any of the source node such that time average is maintain as 200. These flows are active for whole simulation time. On the other hand, mice is consist of small amount of packet each flow having uniform distribution between 3 to 25 packets. The mice flows are created from different nodes such that it captures approximately 5% of total bandwidth at target link.

#### Closed-loop TCP traffic: 
Closed loop traffic is simulated using 675 agents originating from different nodes in the topology. Each of which starts a file transfer, wait for a think duration before starting the next file file transfer. Think duration of each agent having exponential distribution with mean as 5 sec. The file distribution is roughly heavy-tail having distribution as sum of three exponential distributions: one modeling large file transfer (mean: 200 packets), one modeling medium file transfer (mean: 50 packets) and, one modeling small file transfer (mean: 15 packets). Arrival rate is adjusted such that 30% of flows account for 70% of the traffic.

We consider two buffer sizes for simulations (i) evaluated using Standford model (B s )and, (ii) twice of bandwidth-delay product B_l. B_s is calculated as 18 packets and B_l is calculated as 500 packets considering N = 200 long flows. Considering these two buffer lengths simulations are carried out for discussed workload having 600 sec simulation time.

## 4. GENI testbed Simulation
From the Figure of the topology, we can observe that there are five types of nodes (i) sink, (ii) nodes connecting target link, (ii) nodes at one hop from target link, (iv) nodes at two hops from target link (v) nodes at three hops from target link. Rspec is formed such that respective RTT would be assigned automatically on bootup. After bootup of the devices we need to setup all the source nodes and sink, i.e, install iperf3 and get necessary files on the nodes.

####Node setup
To setup sink use following command:
```
wget https://bitbucket.org/rajeevpnj/el7353finalprojet/raw/master/setupsink.sh
bash setupsink.sh 
```
For all the nodes in the topology get corresponding setup files using:  
```
wget https://bitbucket.org/rajeevpnj/el7353finalproject/raw/master/setupnodes.sh
bash setupnodes.sh
```
By using these shell script files, we have installed required software and got required script files on each of our source nodes, which are at one hop, two hop and three hops distant from target link. 

#### Files and supporting files
Now, I will illustrate what these files are and what they do:

**At Sink:** After running the shell script one can observe that there we got two files at sink as,

*receiverCL.sh : it start required number of connections at sink in closed loop experiment case.*

*receiverPer.sh : it start required number of connections at sink in persistent connection with mice experiment case.* 

**At all other source nodes:** 
After running the shell script you can observe few python files and shell script files. Python files are just supporting file. At each source node two main files are there which uses all other supporting files internally. These files are:

*finalCL.sh     : It start TCP flows at each node in closed loop experiment at each node.* 

*finalPer.sh    : It start TCP flow at each node in persistent connection with mice experiment at each node.*
#### Setup buffer at target link
Before beginning the experiment, we need to set buffer length at target2 outgoing port. Find the outgoing port at target2.it will have IP
address ending with 1 as fourth octet. Set buffer size using pfifo:
```
sudo tc qdisc replace dev <Ethernet port> root pfifo limit <buffer length> in packets
```
####Running the experiment
Now, start connections at sink by:
```
bash receiverPer.sh : for persistent experiment
bash receiverCL.sh : for Closed loop experiment
```
At all other nodes run:
```
bash finalPer.sh | tee somename.txt : for persistent experiment
bash finalCL.sh | tee somename.txt : for Closed loop experiment
```
As, we can see from the instruction that we have to run same command at each source nodes. Thus, we use cluster SSH for running the script at all of the source nodes in the topology. To use cluster SSH follow the instruction below:
Install cluster ssh in your system using:
```
sudo apt-get install clusterssh
```
***Using clusterSSH to run the experiment***
```
cssh [options] [user1@]<server1>[:port1] [user2@]<server2>[:port2] ... [userN@]<serverN>[:portN] 
``` 
Here, N is the number of source nodes. Thus, using clusterSSH running the experiment becomes fairly simple. For more guide on using clusterSSH please go through man file of clusterSSH and [link](http://www.unixmen.com/how-to-manage-multiple-ssh-sessions-using-cluster-ssh-and-pac-manager/).
**NOTE:** By using clusterSSH, we can not give different names for textfile at different nodes. However, ClusterSSH makes running the experiment fairly easy. Hence, we stick to it and after running the experiment please get these textfiles at local disk or at same place. Use your intellectuality for changing the files name so that you can write a simple shell script to accumulate all the useful data in a single file.

####Analysis of the result
extract the result from obtained log file using:
```
grep "sender" somename.txt > outputfile.txt
#note that this you have to do for all the file obtained during experiment 
```
Now extract 5th 7th and 9th column: these contains bytes sent, throughput and re-transmissions respectively.
```
awk '{print $5","$7","$9}' outputfile.txt > newfile.txt 
# Here, column 5th, 7th and 9th contains total transmitted bytes, throughput and number of re-transmissions respectively. 
```
Now, we have required information. we can get plots using matlab. we have to obtain CDF plot of each experiment of all the data obtained at 18 nodes. (For details of matlab script please go through it.)

**NOTE:** If you want to check the details of the implementations and to get matlab script files please clone the repository mentioned below,
```
git clone git@bitbucket.org:rajeevpnj/el7353finalproject.git
```
## References

[1] Curtis Villamizar and Cheng Song. High performance TCP in ANSNET. SIGCOMM Comput. Commun. Rev., 24(5):45–60, October 1994.

[2] Guido Appenzeller, Isaac Keslassy, and Nick McKeown. Sizing router buffers. In Proceedings of the 2004 Conference on Applications, Technologies, Architectures, and Protocols for Computer Communications, SIGCOMM ’04, pages 281 292, New York, NY, USA, 2004. ACM.

[3] Amogh Dhamdhere and Constantine Dovrolis. Open issues in router buffer sizing. SIGCOMM Comput. Commun. Rev., 36(1):87–92, January 2006.

[4] Jitendra Padhye, Victor Firoiu, Don Towsley, and Jim Kurose. Modeling TCP throughput: A simple model and its empirical validation. In Proceedings of the ACM SIGCOMM ’98 Conference on Applications, Technologies, Architectures, and Protocols for Computer Communication, SIGCOMM’98, pages 303–314, New York, NY, USA, 1998. ACM.