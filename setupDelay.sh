# High level task: Get list of experiment interfaces and IPs
# parse ifconfig output to get dictionary of interfaces and IP addresses
# eth0 => 10.1.2.1, eth1 => 10.2.1.1
# remove any that don't start with 10
# Reference: https://github.com/dolly115/resilient-networks-lab/
declare -A arr
declare -A brr
 
for i in $(ifconfig | grep "eth" | awk '{print $1;}')
do 
  IP=$(ifconfig $i | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*')
  if [[ $IP == 10.* ]]
  then
    arr["$i"]=$IP
    brr["$IP"]=$i
	# Get second octet of IP address (which is in $IP variable)	
	# store it in a variable called type
	type=`echo $IP | cut -d . -f 2`
	if [ $type -eq 3  ]
	then
		sudo tc qdisc replace dev "$i" root netem delay 150ms rate 100mbit
	elif [ $type -eq 2 ]
	then
		sudo tc qdisc replace dev "$i" root netem delay 100ms rate 100mbit
	elif [ $type -eq 1 ]
	then
		sudo tc qdisc replace dev "$i" root netem delay 10ms rate 100mbit
	elif [ $type -eq 20 ]
	then
		sudo tc qdisc replace dev "$i" root netem delay 5ms rate 50mbit
	else
		sudo tc qdisc replace dev "$i" root netem delay 2ms rate 100mbit
	fi

  fi
done

for key in ${!arr[@]}
do 
  echo ${key} ${arr[${key}]} 
done

