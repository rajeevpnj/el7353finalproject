#!/bin/bash
port=$1
intialTime=$(date +"%s")
pakLen=1500
simulationTime=150
currentTime=$(date +"%s")
runTime=$(($currentTime-$intialTime))
while [[ $runTime -le $simulationTime ]];
do 
	sleeptime=$(shuf -i1-2 -n1)
	sleep $sleeptime
	pack=$(shuf -i3-25 -n1)
	bytes=$(($pack*pakLen))
	iperf -c 10.10.1.1 -p $port -l $pakLen -n $bytes
	currentTime=$(date +"%s")
	runTime=$(($currentTime-$intialTime))
done
