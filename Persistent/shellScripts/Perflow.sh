# eth0 => 10.1.2.1, eth1 => 10.2.1.1
# remove any that don't start with 10
# Reference: https://github.com/dolly115/resilient-networks-lab/
declare -A arr
declare -A brr
 
for i in $(ifconfig | grep "eth" | awk '{print $1;}')
do 
  IP=$(ifconfig $i | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*')
  if [[ $IP == 10.* ]]
  then
    arr["$i"]=$IP
    brr["$IP"]=$i
	# Get second octet of IP address (which is in $IP variable)	
	# store it in a variable called type
	intialTime=$(date +"%s")
	simulationTime=150
	type=`echo $IP | cut -d . -f 2`
	type1=`echo $IP | cut -d . -f 3`
	type2=`echo $IP | cut -d . -f 4`

    if [[ $type2 -eq 1 ]];
    then

        # Find out what kind of node we are (how many hops away)
        if [[ $type -eq 1 ]];
        then
            max_n=13
            port=$((10001+($type1-10)*$max_n))
            miceport=$((10201+($type1-10)*30))
        elif [[ $type -eq 2 ]];
        then
            max_n=12

            my_array=(10 11 20 30 31)

            for i in "${!my_array[@]}"; do
               if [[ "${my_array[$i]}" = "${type1}" ]]; then
                   idx=${i}
               fi
            done
            port=$((10040+$idx*$max_n))
            miceport=$((10291+$idx*30))

        elif [[ $type -eq 3 ]];
        then
            max_n=10

            my_array=(10 11 20 21 30 31 40 41 50 51)

            for i in "${!my_array[@]}"; do
               if [[ "${my_array[$i]}" = "${type1}" ]]; then
                   idx=${i}
               fi
            done
            port=$((10100+$idx*$max_n))
            miceport=$((10441+$idx*30))
        fi

        pakLen=1500
        n=1
        n1=5
        while [[ $n -le $max_n ]];
        do 
            iperf -c 10.10.1.1 -p $port -l $pakLen -t 150 &
            port=$(($port+1))
            n=$(($n+1)) 
        done
        n=1
        while [[ $n -le $n1 ]];
        do 
            bash /flowPer.sh $miceport &
            miceport=$(($miceport+1))
            n=$(($n+1))         
        done

    fi
  fi
done

