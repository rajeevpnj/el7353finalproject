sudo sysctl -w net.ipv4.tcp_congestion_control=reno

#Install iperf
sudo apt-get -y install tcpdump vim iperf
#increase the number of processes that can be handled
sudo bash -c 'echo "* soft nofile 64000" >> /etc/security/limits.conf'
sudo bash -c 'echo "* hard nofile 64000" >> /etc/security/limits.conf'
sudo bash -c 'echo "* soft nproc 8192" >> /etc/security/limits.conf'
sudo bash -c 'echo "* hard nproc 16384" >> /etc/security/limits.conf'
sudo bash -c 'echo "root soft nproc unlimited" >> /etc/security/limits.conf'
#Get all the resources at node18
#test scripts for closed loop
wget https://bitbucket.org/rajeevpnj/el7353finalproject/raw/master/closedLoop/shellScripts/sink/receiverCL.sh
#test script for persistent flow
wget https://bitbucket.org/rajeevpnj/el7353finalproject/raw/master/Persistent/shellScripts/sink/receiverPer.sh
