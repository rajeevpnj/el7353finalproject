data1 = load('buffer18CL.txt'); %data of bufferlength 18
data2 = load('buffer500CL.txt'); %data of bufferlength 500
data18 = []; data500 = [];
% As the report migh be in Mbps or Kbps we are telling if speed in Mbps then convert into Kbps. 
% This we simply do by checking of the magnitude of speed in Mbps which lie typically between 1 to 4 Mbps
%For buffer = 18 packets
for i =1:length(data1)
    if (data1(i,2)>1 && data1(i,2)<10) 
        data18 = [data18; data1(i,1) data1(i,2)*1024 data1(i,3)];
    else
        data18 = [data18;data1(i,:)];
    end
end
%For buffer = 500 packets
for i =1:length(data2)
    if (data2(i,2)>1 && data2(i,2)<10) 
        data500 = [data500; data2(i,1) data2(i,2)*1024 data2(i,3)];
    else
        data500 = [data500;data2(i,:)];
    end
end
% in the there are three column 1st indicating data in KBS, second throughput and third loss 
data18Pack = []; data500Pack = [];
% Converting KBytes to packets for buffer 18
for i =1:length(data18)
    if (data18(i,1) < 10)
        data18Pack = [data18Pack; floor(data18(i,1)*1024*1024/1500) data18(i,2) data18(i,3)];
    else
        data18Pack = [data18Pack; floor(data18(i,1)*1024/1500) data18(i,2) data18(i,3)];
    end
end
% Converting KBytes to packets for buffer 500
for i =1:length(data500)
    if (data500(i,1) < 10)
        data500Pack = [data500Pack; floor(data500(i,1)*1024*1024/1500) data500(i,2) data500(i,3)];
    else
        data500Pack = [data500Pack; floor(data500(i,1)*1024/1500) data500(i,2) data500(i,3)];
    end
end
large18 = []; large500 = []; small18 = []; small500 = [];
% seprating large and small flow for buffer 18
for i =1:length(data18Pack)
    if (data18Pack(i,1)<100)
        small18 = [small18; data18Pack(i,:)];
    else
        large18 = [large18; data18Pack(i,:)];
    end
end
% seprating large and small flow for buffer 500
for i =1:length(data500Pack)
    if (data500Pack(i,1)<100)
        small500 = [small500; data500Pack(i,:)];
    else
        large500 = [large500; data500Pack(i,:)];
    end
end
% cdf plot of large flow
figure(1)
h=cdfplot(large18(:,2)*128);
set(h,'Color','k','LineStyle','-' );
hold on; grid on
h=cdfplot(large500(:,2)*128);
set(h,'Color','b','LineStyle','--');
set(gca,'xscale','log');
legend('B = 18 [Large Flow]','B = 500 [Large Flow]')
xlabel('Flow Throughput [BPS]')
ylabel('CDF')
hold off
% cdf plot of small flow
figure(2)
h=cdfplot(small18(:,2)*128);
set(h,'Color','k','LineStyle','-');
hold on; grid on
h=cdfplot(small500(:,2)*128);
set(h,'Color','b','LineStyle','--');
set(gca,'xscale','log');
legend('B = 18 [Large Flow]','B = 500 [Large Flow]')
xlabel('Flow Throughput [BPS]')
ylabel('CDF')
hold off
% packet loss rate large
figure(3)
h=cdfplot(100*large18(:,3)./large18(:,1));
set(h,'Color','k','LineStyle','-');
hold on; grid on
h=cdfplot(100*large500(:,3)./large500(:,1));
set(h,'Color','b','LineStyle','--');
legend('B = 18 [Large Flow]','B = 500 [Large Flow]')
xlabel('Flow Packloss Rate [%]')
ylabel('CDF')
hold off
% packet loss rate mice
figure(4)
h=cdfplot(100*small18(:,3)./small18(:,1));
set(h,'Color','k','LineStyle','-');
hold on; grid on
h=cdfplot(100*small500(:,3)./small500(:,1));
set(h,'Color','b','LineStyle','--');
legend('B = 18 [Large Flow]','B = 500 [Large Flow]')
xlabel('Flow Packloss Rate [%]')
ylabel('CDF')
hold off
