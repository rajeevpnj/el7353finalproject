#!/bin/bash
numPack=$1
numSleep=$2
port=$3
intialTime=$(date +"%s")
pakLen=1500
simulationTime=150
currentTime=$(date +"%s")
runTime=$(($currentTime-$intialTime))
while [[ $runTime -le $simulationTime ]];
do 
	sleep $(python /expoRandom.py "$numSleep")
	totalPak=$(python /expoRandom.py "$numPack")
	bytes=$(echo "$totalPak"*"$pakLen" | bc)
	iperf -c 10.10.1.1 -p $port -l $pakLen -n $bytes
	currentTime=$(date +"%s")
	runTime=$(($currentTime-$intialTime))
done
