# eth0 => 10.1.2.1, eth1 => 10.2.1.1
# remove any that don't start with 10
# Reference: https://github.com/dolly115/resilient-networks-lab/
declare -A arr
declare -A brr
 
for i in $(ifconfig | grep "eth" | awk '{print $1;}')
do 
  IP=$(ifconfig $i | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*')
  if [[ $IP == 10.* ]]
  then
    arr["$i"]=$IP
    brr["$IP"]=$i
	# Get second octet of IP address (which is in $IP variable)	
	# store it in a variable called type
	intialTime=$(date +"%s")
	simulationTime=150
	type=`echo $IP | cut -d . -f 2`
	type1=`echo $IP | cut -d . -f 3`
	type2=`echo $IP | cut -d . -f 4`

    if [[ $type2 -eq 1 ]];
    then

		#node 1 traffic
		n=1
		n2=20
		n3=7

        # Find out what kind of node we are (how many hops away)
        if [[ $type -eq 1 ]];
        then
            n1=13

            port1=$((10000+($type1-10)*$n1))
            port2=$((10201+($type1-10)*$n2))
            port3=$((10601+($type1-10)*$n3))

        elif [[ $type -eq 2 ]];
        then
            n1=12

            my_array=(10 11 20 30 31)

            for i in "${!my_array[@]}"; do
               if [[ "${my_array[$i]}" = "${type1}" ]]; then
                   idx=${i}
               fi
            done

            port1=$((10039+$idx*$n1))
            port2=$((10261+$idx*$n2))
            port3=$((10622+$idx*$n3))

        elif [[ $type -eq 3 ]];
        then
            n1=10

            my_array=(10 11 20 21 30 31 40 41 50 51)

            for i in "${!my_array[@]}"; do
               if [[ "${my_array[$i]}" = "${type1}" ]]; then
                   idx=${i}
               fi
            done

            port1=$((10099+$idx*$n1))
            port2=$((10361+$idx*$n2))
            port3=$((10657+$idx*$n3))
        fi

		while [[ $n -le $n1 ]];
		do
			bash /flow.sh 200 5 $port1 &
			port1=$(($port1+1))
			n=$(($n+1))
		done
		n=1
		while [[ $n -le $n2 ]];
		do
			bash /flow.sh 50 5 $port2 &
			port2=$(($port2+1))
			n=$(($n+1))
		done
		n=1
		while [[ $n -le $n3 ]];
		do
			bash /flow.sh 15 5 $port3 &
			port3=$(($port3+1))
			n=$(($n+1))
		done
	fi
  fi
done
